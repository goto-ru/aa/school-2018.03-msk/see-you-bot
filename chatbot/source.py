# -*- coding: utf-8 -*-
from secrets import token_hex
from telebot import TeleBot
from phrase_generator import generate_phrase, conclusion
from understander import check_desire
import config

bot = TeleBot(config.token, threaded=False)

states = {}
desire_mask = {}
user_codes = {}
code_owners = {}
codes_activated = {}
locations = {}


states_cycle = ['start', 'friend_code', 'geo', 'park', 'rest', 'museum', 'end']


def generate(msg):
    status = understand(msg)
    if status == 1:
        return generate_phrase('no_understanding')
    if states[msg.chat.id] == 'end':
        token = user_codes[msg.chat.id]

        if len(code_owners[token]) < 2:
            states[msg.chat.id] = 'waiting'
            return generate(msg)

        friend = code_owners[token][0] if code_owners[token][0] != msg.chat.id else code_owners[token][1]

        if len(desire_mask[friend]) == 3:
            final_mask = [
                desire_mask[friend][0] + desire_mask[msg.chat.id][0],
                desire_mask[friend][1] + desire_mask[msg.chat.id][1],
                desire_mask[friend][2] + desire_mask[msg.chat.id][2]
            ]
            
            bot.send_message(msg.chat.id, 'Производим сложные вычисления...')
            conclude = conclusion(final_mask, locations[msg.chat.id], locations[friend])
            bot.send_message(friend, conclude)
            states[friend] = 'end'

            del user_codes[friend]
            del user_codes[msg.chat.id]
            del codes_activated[token]
            del code_owners[token]
            del desire_mask[friend]
            del desire_mask[msg.chat.id]
            del locations[msg.chat.id]

            return conclude

        states[msg.chat.id] = 'waiting'
        generate(msg)
    else:
        return generate_phrase(states[msg.chat.id])


def understand(msg):
    state = states[msg.chat.id]
    print('state '+state)
    if state == 'start':
        states[msg.chat.id] = 'friend_code'
        desire_mask[msg.chat.id] = []
    elif state == 'friend_code':
        for line in msg.text.split('\n'):
            if line.startswith('BOTCODE_'):
                if line in user_codes.values():
                    codes_activated[line] = True
                    user_codes[msg.chat.id] = line
                    code_owners[line].append(msg.chat.id)
                    bot.send_message(msg.chat.id, 'Код верный!')
                    states[msg.chat.id] = 'geo'
                    return 0
                else:
                    return 1
        token = 'BOTCODE_'+token_hex(6)
        user_codes[msg.chat.id] = token
        code_owners[token] = [msg.chat.id]
        codes_activated[token] = False
        bot.send_message(msg.chat.id, 'Отправь этот код своему другу!\n{}'.format(token))
        states[msg.chat.id] = 'geo'
    elif state == 'geo':
        if msg.location:
            print('got geo')
            locations[msg.chat.id] = (msg.location.latitude, msg.location.longitude)
            states[msg.chat.id] = 'park'
            return 0
        return 1
    elif state in ['park', 'rest', 'museum']:
        status = check_desire(msg)
        if status == -1:
            print('didnt get:\n'+msg.text)
            return 1
        states[msg.chat.id] = states_cycle[states_cycle.index(state)+1]
        if status == 0:
            bot.send_message(msg.chat.id, generate_phrase('wants'))
            desire_mask[msg.chat.id].append(True)
        else:
            bot.send_message(msg.chat.id, generate_phrase('doesnt_want'))
            desire_mask[msg.chat.id].append(False)
    elif state == 'end':
        states[msg.chat.id] = 'friend_code'
        desire_mask[msg.chat.id] = []
    return 0


@bot.message_handler(content_types=['location'])
def handle_location(message):
    bot.send_message(message.chat.id, generate(message))


@bot.message_handler(func=lambda msg:True)
def process_msg(msg):
    print('got msg!')
    if msg.chat.id not in states:
        states[msg.chat.id] = 'start'
        bot.send_message(msg.chat.id, 'Привет! Хороший день, чтобы сходить куда-то с друзьями c:')
        
    bot.send_message(msg.chat.id, generate(msg))


def run():
    bot.polling(none_stop=True)


if __name__ == '__main__':
    run()