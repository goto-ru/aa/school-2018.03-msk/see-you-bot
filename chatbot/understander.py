# -*- coding: utf-8 -*-
import string


class Understander:
    def __init__(self):
        self.types = {
            'POS_PART': ['да', 'дат', 'yup', 'yes', 'угу', 'ок', 'ok'],
            'NEG_PART': ['нет', 'неа', 'ноуп', 'ноу', 'no', 'nope'],
            'VERB': ['люблю', 'хочу', 'обожаю', 'тащусь', 'хотел', 'хотелось',
                    'круто', 'хорошо', 'супер', 'жизнь', 'интересно','увлекаюсь',
                    'радостью', 'неплохо', 'давай', 'гоу', 'суперски', 'прекрасно', 'давай'],
            'NEG_VERB': ['ненавижу', 'терпеть', 'отстой', 'отстойно', 'плохо', 'фу',
                        'такое', 'гадость', 'ничего'],
            'V_PART': ['не', 'ни'],
            'RUBBISH': ['бы', 'с', 'для', 'о'],
        }

    def check_word_type(self, word):
        for tp in self.types:
            if word in self.types[tp]:
                return tp
        return 'UNKNWN'

    def check_desire(self, msg):
        """
        1 - understood, doesn't want
        0 - understood, wants
        -1 - didn't understand
        """
        text = "".join(l for l in msg.text if l not in string.punctuation).lower().split(' ')
        negative = False
        result = -1

        for word in text:
            tp = self.check_word_type(word)
            print('{} classified as {}'.format(word, tp))
            if tp == 'POS_PART':
                result = 0
            elif tp == 'NEG_PART':
                result = 1
            if tp == 'VERB':
                if negative:
                    result = 1
                result = 0
            if tp == 'NEG_VERB':
                if negative:
                    result = 0
                result = 1
            if tp == 'V_PART':
                negative = not negative
            if tp == 'RUBBISH':
                continue
        if negative:
            result = 1
            
        return result

understander = Understander()

def check_desire(msg):
    return understander.check_desire(msg)