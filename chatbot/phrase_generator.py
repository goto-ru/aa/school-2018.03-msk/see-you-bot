# -*- coding: utf-8 -*-
import random
import re
from city_mapping import geo_to_closest


terminals = {
    'no_understanding':
        ['Прости, я не понимаю тебя :c',
         'Я не очень тебя понял, давай попробуем ещё раз.',
         'Я в замешательстве. Попробуй ещё разок.',
         'Хм... Я даже не знаю, что сказать.'],
    'park':
        ['<DESIRE> <PARK>?'],
    'rest':
        ['<DESIRE> <EAT>?'],
    'museum':
        ['<DESIRE> <MUSEUM>?'],
    'end':
        ['<MOVE> {}!'],
    'places': ['парк', 'ресторан', 'музей'],
    'doesnt_want': ['Значит в следующий раз.', 'Ну, как хочешь.', 'Придумаем что-нибудь еще.'],
    'wants': ['Супер!', 'Великолепно!', 'Прекрасно!', 'Прелестно!'],
    'friend_code': ['Введи код, отправленный другом, или отправь любое слово, чтобы продолжить!'],
    'waiting': ['Ждём, пока твой друг ответит на вопросы!'],
    'geo': ['Отправь свою геолокацию!', 'Поделись своим местоположением!'],
    '<DESIRE>': ['Хочешь', 'Хотелось бы тебе', 'Не хотелось бы тебе', 'Что насчёт', 'Не хочешь', 'Как тебе идея',
                 'Как насчёт'],
    '<MOVE>': ['Сегодня мы идём в', 'Отправляемся в', 'Поехали в', 'Самое время поехать в', 'Пора собираться в'],
    '<PARK>': ['прогуляться на свежем воздухе', 'провести время на улице', 'сходить в парк'],
    '<EAT>': ['перекусить', 'немного поесть', 'отведать вкуностей'],
    '<MUSEUM>': ['сходить в музей', 'посетить один из прекрасных московских музеев',
                 'отправиться в один из музеев Москвы'],
    'wants_nothing': ['Великий нехочуха :c\n(и ты и твой друг)\n\nОтправь любое сообщение, чтобы начать заново.']
}


def generate_phrase(status):
    phrase = random.choice(terminals[status])
    phrase = open_terminals(phrase)        
    return phrase


def open_terminals(text):
    term = re.findall("<[^<]*>", text)
    final_text = text
    if len(term):
        final_text = text.replace(term[0], random.choice(terminals[term[0]]))
    if final_text != text:
        return open_terminals(final_text)
    return final_text


def conclusion(desire_mask, geo1, geo2):
    phr = generate_phrase('end')

    try:
        place = random.choices(terminals['places'], weights=desire_mask, k=1)
        print(place)
    except IndexError:
        return generate_phrase('wants_nothing')
    if len(place) == 0:
        return generate_phrase('wants_nothing')
        
    station = geo_to_closest(geo1, geo2, place[0])
    return phr.format(place[0]) + '\n\nЛучшее место для вашей встречи - {}\n\nОтправь любое сообщение, чтобы начать заново.'.format(station)
