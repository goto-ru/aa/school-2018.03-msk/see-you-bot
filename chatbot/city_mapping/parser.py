# -*- coding: utf-8 -*-
from requests_html import HTMLSession
import requests
import re
import random
import pickle
from bs4 import BeautifulSoup
import json
from geopy.distance import vincenty
import googlemaps
from .config import token


class Node:
    x = 0

    def __init__(self, name, coordinates, tag=None):
        self.id = Node.x
        Node.x += 1
        self.name = name
        self.coordinates = coordinates
        self.tag = tag
        self.neighbours = []

    def dist(self, geo):
        return vincenty(self.coordinates, geo).meters

    def __repr__(self):
        return '{}: {}'.format(self.name, str(self.coordinates))


class Parser:
    def __init__(self):
        self.token = token
        self.gmaps = googlemaps.Client(key=self.token)
        self.nodes = None
        self.ids = None
    
    def get_subways(self, redo=False):
        if self.nodes is not None:
            return self.nodes
            
        if not redo:
            try:
                with open('metros.pickle', 'rb') as f:
                    nodes = pickle.load(f)
                    self.nodes = nodes
                    return nodes
            except FileNotFoundError:
                pass

        not_implemented = ['Улица Милашенкова', 'Телецентр', 'Улица Академика Королёва', 
                           'Выставочный центр', 'Улица Сергея Эйзенштейна'] # Not yet in metro scheme and not opened yet

        request = requests.get('https://api.hh.ru/metro/1')
        text = request.text
        data = json.loads(text)
        subways = []
        
        for line in data['lines']:
            for station in line['stations']:
                name = station['name'].rstrip().replace('им.', 'им. ')
                if name not in not_implemented:
                    subways.append([name, (station['lat'], station['lng'])])

        nodes = []

        for station_entrance in subways:
            nd = Node(station_entrance[0], station_entrance[1], tag='subway')
            nodes.append(nd)

        with open('metros.pickle', 'wb') as f:
            pickle.dump(nodes, f)
        self.nodes = nodes

        return nodes


    def get_closest_subway(self, pos):
        nodes = self.get_subways()
        min_node = None

        for node in nodes:
            dist = node.dist(pos)
            if min_node is None:
                min_node = node
                continue
            if dist < min_node.dist(pos):
                min_node = node

        return min_node


    def get_metro_ids(self, redo=False):
        if self.ids is not None:
            return self.ids
            
        if not redo:
            try:
                with open('ids.pickle', 'rb') as f:
                    ids =  pickle.load(f)
                    self.ids = ids
                    return ids
            except FileNotFoundError:
                pass
                
        response = requests.get('https://kartametro.ru')
        html = response.text
        soup = BeautifulSoup(html, 'lxml')
        stations = {}

        for i in range(0, 245):
            station = str(soup.find('text', {'data-index': i}))

            for tag in re.findall("<[^<]*>", station):
                station = station.replace(tag, '')

            replacements = [('\xa0', ' '), ('пл.', 'площадь'), ('ё', 'е'), ('1905', ' 1905'), ('ул.', 'улица'),
                            ('верхние', 'верхние '), ('адмирала', 'адмирала '), ('соколиная', 'соколиная ')]
            for r in replacements:
                station = station.replace(r[0], r[1])

            stations[station] = i

        stations['селигерская'] = 243 # Not in API
        stations['верхние лихоборы'] = 244

        with open('ids.pickle', 'wb') as f:
            pickle.dump(stations, f)

        self.ids = stations
            
        return stations


    def get_metro_route(self, st1, st2):
        real_st1 = str(st1)
        while len(real_st1) < 3:
            real_st1 = '0' + real_st1

        real_st2 = str(st2)
        while len(real_st2) < 3:
            real_st2 = '0' + real_st2
            
        url = 'https://kartametro.ru/?p={}{}'.format(real_st1, real_st2)
        session = HTMLSession()
        response = session.get(url)
        response.html.render()
        soup = BeautifulSoup(response.html.html, 'lxml')
        
        time = str(soup.find('div', {'class': 'js-time-container time-container'}))

        for tag in re.findall("<[^<]*>", time):
            time = time.replace(tag, '')
        try:
            time = time.split(' ')[1]
        except IndexError:
            return -1
            
        return int(time)


    def get_middle_station(self, st1, st2):
        real_st1 = str(st1)
        while len(real_st1) < 3:
            real_st1 = '0' + real_st1
        real_st2 = str(st2)
        while len(real_st2) < 3:
            real_st2 = '0' + real_st2
            
        url = 'https://kartametro.ru/?p={}{}'.format(real_st1, real_st2)
        session = HTMLSession()
        response = session.get(url)
        response.html.render()
        soup = BeautifulSoup(response.html.html, 'lxml')
        hidden_layer = soup.find('g', {'class': 'js-svg-path-layer svg-path-layer'}).findAll('text')

        station = str(hidden_layer[int((len(hidden_layer)-1)/2)])

        for tag in re.findall("<[^<]*>", station):
            station = station.replace(tag, '').lower()

        replacements = [('\xa0', ' '), ('пл.', 'площадь'), ('ё', 'е'), ('1905', ' 1905'), ('ул.', 'улица'),
                        ('верхние', 'верхние '), ('адмирала', 'адмирала '), ('соколиная', 'соколиная ')]
        for r in replacements:
            station = station.replace(r[0], r[1])
        
        all_stations = self.get_subways()
        for subway in all_stations:
            if subway.name.lower() == station:
                return subway


    def get_nearby_places(self, location, place_type):
        if place_type == 'парк':
            ptype = ['park']
        elif place_type == 'ресторан':
            ptype = ['cafe', 'restaurant']
        else:
            ptype =['museum']

        places = self.gmaps.places_nearby(location=location, type=random.choice(ptype), rank_by="distance", language='ru')
        return '{} по адресу {}'.format(places['results'][0]['name'], places['results'][random.randint(0, 5)]['vicinity'])


parser = Parser()


def geo_to_closest(geo1, geo2, place):
    metro1 = parser.get_closest_subway(geo1)
    metro2 = parser.get_closest_subway(geo2)

    print(metro1)
    print(metro2)

    if metro1.name == metro2.name:
        return parser.get_nearby_places(metro1.coordinates, place)

    ids = parser.get_metro_ids()
    station = parser.get_middle_station(ids[metro1.name.lower()], ids[metro2.name.lower()])

    return parser.get_nearby_places(station.coordinates, place)
