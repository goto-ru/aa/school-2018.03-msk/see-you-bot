from collections import deque, defaultdict


def bfs(start, graph):
    d = defaultdict()
    mark = defaultdict()
    q = deque()
    q.append(start)
    d[start] = 0
    mark[start] = 1

    while len(q) > 0:
        h = q.popleft()
        for nbr in graph[h]:
            if nbr not in mark:
                if d[nbr] > d[h]+1:
                    d[nbr] = d[h]+1
                mark[nbr] = 1
                q.append(nbr)

    return d