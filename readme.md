# SeeYouBot - a bot for organising meetings in real time!
![see-you-logo](logo_small.png "Check out our logo!")
## Description:
Meet your friends in real life without any delay!  
Organise any meeting in a blink of an eye using SeeYouBot!  
Automatically get the most suitable place to meet and enjoy your day!  
    
Two people share their geolocations with the bot, and then have a short dialogue.  
Preferences are detected throughout the dialogue, and the best place to meet  
is proposed based on preferences and geolocations of both users.  
  
Bot's current language is russian.  
  
**Roadmap**  
- Basic prototype [done!]
- Better routing & worktime optimization
- English language support
- Approxiamte arrival time


## Code structure:
**./chatbot/** - main chatbot dir. Understander & generator modules.  
Run **./chatbot/source.py** to actually run the bot.  
  
**./chatbot/parser/** - all geo data processing.  
Mostly in **parser.py**.  

## Presentation
[click!](https://docs.google.com/presentation/d/1iqzdnfYBsZ1XfiUIfVPIqQspdTeaLLNw61A6ZVBNJXA/edit?usp=sharing)

## Requirements:
- Python3.6+
- pytelegrambotapi, googlemaps, geopy, bs4
  

Voronkov Alexander, 2018  
Telegram: @lifetimealone